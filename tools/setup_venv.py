#!/usr/bin/python3

import subprocess as sp
import click
from abc import ABCMeta, abstractmethod
from IPy import IP
from pyroute2 import IPDB
from pyroute2 import NetNS
from pyroute2 import NSPopen
import logging
import tempfile
from logging import info, warning, debug, error, critical
from collections import OrderedDict
import signal
import time
from os import path
import os
import time
from typing import Optional
import sys, inspect
import re
from pprint import pprint

__author__ = "Fabian Raab"
__email__ = "fabian.raab@tum.de"

MAC80211_HWSIM = "mac80211_hwsim"

MODINFO_BIN = "modinfo"
MODPROBE_BIN = "modprobe"
LSMOD_BIN = "lsmod"
GREP_BIN = "grep"
WMEDIUMD_BIN = "wmediumd"
home = path.expanduser("~")
script_dir = path.dirname(path.realpath(__file__))
ncm_bin = path.join(script_dir, os.pardir, "moep80211ncm", "ncm")

logging.basicConfig(format="%(asctime)-15s %(levelname)-7s %(funcName)-16s %("
                           "message)s", level=logging.WARNING)

wmediumd_log_level = 7


def is_module_available(name: str) -> bool:
    debug("check if module " + name + "is available with '" + MODINFO_BIN + " "
          + name + "'")
    ps_modinfo = sp.run([MODINFO_BIN, name],
                        stdout=sp.DEVNULL,
                        stderr=sp.DEVNULL)
    return not bool(ps_modinfo.returncode)


def is_module_loaded(name: str) -> bool:
    ps_lsmod = sp.Popen((LSMOD_BIN), stdout=sp.PIPE)
    try:
        debug("search if '" + MAC80211_HWSIM +
              "' alread loaded. run \"lsmod | grep " + MAC80211_HWSIM + '"')
        grep_out = sp.check_output((GREP_BIN, name), stdin=ps_lsmod.stdout)
        return True
    except sp.CalledProcessError:
        return False
    finally:
        ps_lsmod.wait()


def load_modules(radios: int = 2, loaded_ok=True) -> None:
    if not is_module_loaded(MAC80211_HWSIM):
        args = (MODPROBE_BIN, MAC80211_HWSIM, "radios=" + str(radios))
        ps_modprobe = sp.run(args)
        info("load '" + MAC80211_HWSIM + "' module")
        debug("run \"" + ' '.join(args) + '"')
    elif not loaded_ok:
        error("Error: module " + MAC80211_HWSIM + " already loaded.")
        raise SystemExit(2)


class Topology(object, metaclass=ABCMeta):
    def __enter__(self) -> object:
        self.got_signal = None

        def handler_int(signum, frame):
            warning("SIGINT received: shutting down ...")
            self.got_signal = signal.SIGINT
            self.stop()

        signal.signal(signal.SIGINT, handler_int)

        def handler_terminate(signum, frame):
            warning("SIGTERM received: forcing processes to die ...")
            self.got_signal = signal.SIGTERM
            self.terminate()

        signal.signal(signal.SIGTERM, handler_terminate)

        return self

    @abstractmethod
    def start(self) -> None:
        pass

    def __exit__(self, type, value, tb) -> None:
        if not (self.got_signal == signal.SIGINT or
                        self.got_signal == signal.SIGTERM):
            self.stop()

        self.teardown()

    @abstractmethod
    def close(self):
        pass

    @abstractmethod
    def teardown(self):
        """must close all open files"""
        pass

    @abstractmethod
    def wait(self):
        pass

    @abstractmethod
    def stop(self) -> None:
        pass

    @abstractmethod
    def terminate(self) -> None:
        pass

    @abstractmethod
    def clean(self) -> None:
        pass


class DefaultTopology(Topology, metaclass=ABCMeta):
    """Starts node, ncm and wmediumd, but it do not specify a topology"""

    def __init__(self):

        self.ns = OrderedDict()
        """"network name spaces"""
        for ns_str in self.ns_names:
            self.ns[ns_str] = NetNS(ns_str)
            debug("create network ns '" + ns_str + "'")

        self.frequency = "2412M"

        self.network = IP("10.0.1.0/24")
        self.mac_base = "02:00:00:0a:00:"
        self.mac_base_wlan = "02:00:00:00:"
        self.log_dir = path.join(script_dir, "log")

        self.nc_ps = OrderedDict()
        """network coding processes"""
        for ns_str in self.ns_names:
            self.nc_ps[ns_str] = None

        self.bin = ncm_bin
        self.bin_args = ["--ccack", "--gensize", "16"]

        self.wmediumd_ps = None
        self.wmediumd_config_file = None

    def start(self):
        load_modules(radios=self.ns_len)
        with IPDB() as ipdb:
            for i in range(self.ns_len):
                mac = self.mac_base_wlan + "{:02d}:00".format(i)
                with ipdb.interfaces["wlan" + str(i)] as wlan:
                    if wlan.address != mac:
                        error_str = (
                            "interface '" + wlan.ifname + "' (" +
                            wlan.address + ") should be created by " +
                            MAC80211_HWSIM + " with hw address " + mac +
                            ", but is not. Are there any existing wireless "
                            + "interfaces with that name probably?")
                        error(error_str)
                        raise SystemExit(2)

        os.makedirs(self.log_dir, exist_ok=True)

        time_str = time.strftime("%a, %d %b %Y %H:%M:%S +0000")
        separator = "-----------"

        ### start wmediumd ###

        with tempfile.NamedTemporaryFile(
                mode='w', suffix=".conf",
                delete=False) as self.wmediumd_config_file:
            info("write wmediumd config to file '" +
                 self.wmediumd_config_file.name + "'")
            self.wmediumd_config_file.write(self.wmediumd_config)

        log_file = open(path.join(self.log_dir, "wmediumd.log"), 'a')
        # write sperator line since the the log is always appended
        log_file.writelines(("\n", separator + ' ' + 'wmediumd: ' +
                             time_str + ' ' + separator + '\n'))
        log_file.flush()

        global wmediumd_log_level
        self.wmediumd_ps = sp.Popen((WMEDIUMD_BIN,
                                     "-c", self.wmediumd_config_file.name,
                                     "-l", str(wmediumd_log_level)),
                                    stdout=log_file, stderr=log_file)
        info("wireless simulator \"" + ' '.join(self.wmediumd_ps.args)
             + "\" started with PID " + str(self.wmediumd_ps.pid))

        self.wmediumd_ps.log_file = log_file
        debug("write output of PID " + str(self.wmediumd_ps.pid) +
              " to file '" + str(log_file.name) + "'")

        ### start network coding programs ###

        for i, (ns_str, _) in enumerate(self.nc_ps.items()):
            log_file = open(path.join(self.log_dir, ns_str + ".log"), 'a')

            # write sperator line since the the log is always appended
            log_file.writelines(("\n", separator + ' ' + ns_str + ': ' +
                                 time_str + ' ' + separator + '\n'))
            log_file.flush()
            mac = self.mac_base + "{:02d}".format(i + 1)
            self.nc_ps[ns_str] = sp.Popen((self.bin, "--hwaddr", mac,
                                           *self.bin_args,
                                           "wlan" + str(i), self.frequency),
                                          stdout=log_file, stderr=log_file)
            info("module \"" + ' '.join(self.nc_ps[ns_str].args)
                 + "\" started with PID " + str(self.nc_ps[ns_str].pid))

            self.nc_ps[ns_str].log_file = log_file
            debug("write output of PID " + str(self.nc_ps[ns_str].pid) +
                  " to file '" + str(log_file.name) + "'")

            attempts = 0
            debug("assume interface 'tap" + str(i) + "' with hw address " + mac)
            sleep_time = 0.1  # seconds
            max_attempts = 6

            while attempts < max_attempts:
                try:
                    with IPDB() as ipdb, ipdb.interfaces[
                                "tap" + str(i)] as tap:
                        debug("assumption: found interface '" + tap.ifname +
                              "' with hw address " + tap.address)
                        if tap.address != mac:
                            error_str = (
                                "interface '" + tap.ifname + "' (" +
                                tap.address + ") should be created by " +
                                self.bin + " with hw address " + mac +
                                ", but is not!")
                            critical(error_str)
                            raise Exception(error_str)
                    break
                except KeyError:
                    attempts += 1
                    warning("No 'tap" + str(i) + "'. Checking again in " +
                            str(sleep_time) + " seconds ...")
                    time.sleep(sleep_time)
            if attempts >= max_attempts:
                critical(
                    "Still no tap" + str(i) + "' device present. giving up!")
                raise SystemExit(2)

        ### configure Network Namespaces ###

        with IPDB() as ipdb:
            for i, (ns_str, ns) in enumerate(self.ns.items()):
                tap_str = "tap" + str(i)
                with ipdb.interfaces[tap_str] as tap:
                    tap.net_ns_fd = ns_str
                    debug("add '" + tap_str + "' (" + tap.address +
                          ") to network ns '" + ns_str + "'")

        for i, (ns_str, ns) in enumerate(self.ns.items()):
            tap_str = "tap" + str(i)
            with IPDB(nl=ns) as ipdb:
                with ipdb.interfaces[tap_str] as tap:
                    ip_str = self.network[i + 1].strNormal(0)
                    prefix = self.network.prefixlen()

                    tap.add_ip(ip_str, prefix)
                    info("add IP " + ip_str + '/' + str(prefix) +
                         " to '" + tap_str + "' (ns: " + ns_str + ", hw: " +
                         tap.address + ")")

                    tap.up()
                    debug("set " + tap_str + " (ns: " + ns_str + ", hw: " +
                          tap.address + ") up")

        warning(
            "Setup of Topology '" + self.__class__.__name__ + "' is ready to "
                                                              "go!")

    def wait(self):
        for _, ps in self.nc_ps.items():
            if ps:
                ps.wait()
        if self.wmediumd_ps:
            self.wmediumd_ps.wait()

    def stop(self):
        for ns_str, ps in self.nc_ps.items():
            if ps:
                debug("send SIGINT (signal 2) to PID " + str(ps.pid) +
                      " in namespace '" + ns_str + "'")
                ps.send_signal(signal.SIGINT)
                ps.log_file.close()

        if self.wmediumd_ps:
            self.wmediumd_ps.send_signal(signal.SIGINT)
            debug("send SIGINT (signal 2) to wmediumd (PID " +
                  str(self.wmediumd_ps.pid) + ")")

    def close(self):
        for ns_str, ps in self.nc_ps.items():
            if ps:
                ps.log_file.close()

        if self.wmediumd_ps:
            self.wmediumd_ps.log_file.close()

    def teardown(self):
        self.close()
        if (self.wmediumd_config_file):
            os.remove(self.wmediumd_config_file.name)

    def terminate(self):
        # TODO only stop if processes are created
        for ns_str, ps in self.nc_ps.items():
            if ps:
                debug("send SIGTERM (signal 15) to PID " + str(ps.pid) +
                      " in namespace '" + ns_str + "'")
                ps.terminate()
                ps.log_file.close()

        if self.wmediumd_ps:
            self.wmediumd_ps.terminate()
            self.wmediumd_ps.log_file.close()

    def clean(self):
        if is_module_loaded(MAC80211_HWSIM):
            info("remove " + MAC80211_HWSIM + " module")
            args = (MODPROBE_BIN, "--remove", MAC80211_HWSIM)
            debug("run \"" + ' '.join(args) + '"')
            sp.run(args)

        self.clean_ns()

    def clean_ns(self):
        info("remove network ns " + ', '.join(
            a.netns for _, a in self.ns.items()))
        for k, v in self.ns.items():
            v.remove()


wmediumd_config_pair = """
ifaces :
{
        ids = [
                "02:00:00:0a:00:01",
                "02:00:00:0a:00:02"
        ];
};
model:
{
        type = "prob";
        default_prob = 1.0;
        links = (
                (0, 1, 0.150000),
                (1, 0, 0.150000)
        );
};
"""


# (1, 2, 0.150000),
# (2, 1, 0.150000)

class Pair(DefaultTopology):
    """
        (1)-------(2)
        alice     bob

    All links are bidirectional.
    The last byte of the MAC address and the IPv4 address is the node number.
    Error probability on each arc is 15%.
    """

    def __init__(self):
        self.ns_names = ("alice", "bob")
        self.ns_len = len(self.ns_names)

        super().__init__()

        self.wmediumd_config = wmediumd_config_pair
        """Temporary storage for config file of type File"""


wmediumd_config_tri = """
ifaces :
{
        ids = [
                "02:00:00:0a:00:01",
                "02:00:00:0a:00:02",
                "02:00:00:0a:00:03"
        ];
};
model:
{
        type = "prob";
        default_prob = 1.0;
        links = (
                (0, 1, 0.150000),
                (1, 0, 0.150000),
                (1, 2, 0.150000),
                (2, 1, 0.150000)
        );
};
"""


# (1, 2, 0.150000),
# (2, 1, 0.150000)

class Tri(DefaultTopology):
    """
        (1)----(2)----(3)
        src    fw     dst

    All links are bidirectional.
    The last byte of the MAC address and the IPv4 address is the node number.
    Error probability on each arc is 15%.
    """

    def __init__(self):
        self.ns_names = ("src", "fw", "dst")
        self.ns_len = len(self.ns_names)

        super().__init__()

        self.wmediumd_config = wmediumd_config_tri
        """Temporary storage for config file of type File"""


wmediumd_config_diamond = """
ifaces :
{
        ids = [
                "02:00:00:0a:00:01",
                "02:00:00:0a:00:02",
                "02:00:00:0a:00:03",
                "02:00:00:0a:00:04"
        ];
};
model:
{
        type = "prob";
        default_prob = 1.0;
        links = (
                (0, 1, 0.150000),
                (1, 0, 0.150000),
                (0, 2, 0.150000),
                (2, 0, 0.150000),

                (1, 2, 0.150000),
                (2, 1, 0.150000),

                (1, 3, 0.150000),
                (3, 1, 0.150000),
                (2, 3, 0.150000),
                (3, 2, 0.150000)
        );
};
"""


class Diamond(DefaultTopology):
    """
          (2)
         / | \\
      (1)  | (4)
         \\ | /
          (3)

    Nodes names are "left", "up", "down", "right".
    All links are bidirectional.
    The last byte of the MAC address and the IPv4 address is the node number.
    Error probability on each arc is 15%.
    """

    def __init__(self):
        self.ns_names = ("left", "up", "down", "right")
        self.ns_len = len(self.ns_names)

        super().__init__()

        self.wmediumd_config = wmediumd_config_diamond
        """Temporary storage for config file of type File"""


wmediumd_config_hexagon = """
ifaces :
{
        ids = [
                "02:00:00:0a:00:01",
                "02:00:00:0a:00:02",
                "02:00:00:0a:00:03",
                "02:00:00:0a:00:04",
                "02:00:00:0a:00:05",
                "02:00:00:0a:00:06"
        ];
};
model:
{
        type = "prob";
        default_prob = 1.0;
        links = (
                (0, 1, 0.150000),
                (1, 0, 0.150000),
                (0, 2, 0.150000),
                (2, 0, 0.150000),

                (1, 3, 0.150000),
                (3, 1, 0.150000),
                (2, 4, 0.150000),
                (4, 2, 0.150000),

                (1, 4, 0.150000),
                (4, 1, 0.150000),
                (2, 3, 0.150000),
                (3, 2, 0.150000),

                (3, 5, 0.150000),
                (5, 3, 0.150000),
                (4, 5, 0.150000),
                (5, 4, 0.150000)
        );
};
"""


class Hexagon(DefaultTopology):
    """
          (2)---(4)
         /   \ /   \\
       (1)    X    (6)
         \\   / \\   /
          (3)---(5)

    Nodes names are "hex1", "hex2", "hex3", "hex4", "hex5", "hex6".
    All links are bidirectional.
    The last byte of the MAC address and the IPv4 address is the node number.
    Error probability on each arc is 15%.
    """

    def __init__(self):
        self.ns_names = ("hex1", "hex2", "hex3", "hex4", "hex5", "hex6")
        self.ns_len = len(self.ns_names)

        super().__init__()

        self.wmediumd_config = wmediumd_config_hexagon
        """Temporary storage for config file of type File"""


topo_cls = Topology

clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
avilable_topos = []
for name, cls in clsmembers:
    if (issubclass(cls, Topology) and not inspect.isabstract(cls) and
                cls not in (DefaultTopology,)):
        avilable_topos.append(name)


@click.group()
@click.option("--topology", "-t", type=click.Choice(avilable_topos),
              required=True)
@click.option("--verbosity", type=click.Choice(("debug", "info", "warning",
                                                "error", "critical")),
              default="warning")
@click.option("--verbose", "-v", count=True, help="Supports counting.")
@click.option("--quiet", "-q", count=True, help="Supports counting.")
def cli(topology: str, verbosity: str, verbose: Optional[int],
        quiet: Optional[int]) -> None:
    global topo_cls
    topo_cls = globals()[topology]
    if not is_module_available(MAC80211_HWSIM):
        critical("Error module " + MAC80211_HWSIM + " not available.")
        raise SystemExit(2)

    level = logging.getLevelName(verbosity.upper())
    debug("set log level to " + str(level) + " (" + verbosity.upper() + ")")

    if verbose:
        nr_map = {
            1: "INFO",
            2: "DEBUG",
        }
        level = logging.getLevelName(nr_map[verbose])
        debug("set log level to " + str(level) + " (" + nr_map[verbose] + ")")

    if quiet:
        nr_map = {
            1: "ERROR",
            2: "CRITICAL",
        }
        level = logging.getLevelName(nr_map[quiet])
        debug("set log level to " + str(level) + " (" + nr_map[quiet] + ")")

    global wmediumd_log_level

    if level >= 50:
        wmediumd_log_level = 1
        wmediumd_log_level_str = "nothing is logged"
    elif level >= 40:
        wmediumd_log_level = 3
        wmediumd_log_level_str = "startup msgs are logged"
    elif level >= 30:
        wmediumd_log_level = 5
        wmediumd_log_level_str = "errors are logged"
    elif level >= 20:
        wmediumd_log_level = 6
        wmediumd_log_level_str = "dropped packets are logged"
    else:
        wmediumd_log_level = 7
        wmediumd_log_level_str = "all packets will be logged"

    debug("wmediumd log level is " + str(wmediumd_log_level) + " '" +
          wmediumd_log_level_str + "'")

    log = logging.getLogger()
    log.setLevel(level)


@cli.command(help="Run a the specified topology and make network namespaces "
                  "available. Supports SIGINT (CTRL-C) and SIGTERM. Run clean "
                  "of that topology before you switch to another one.")
def run() -> None:
    with topo_cls() as topo:
        topo.start()
        topo.wait()


@cli.command(help="Make cleanup of a specific topology which includes deletion "
                  "of namespaces and undloading the kernel module.")
def clean() -> None:
    topo = topo_cls()
    topo.clean()


@cli.command(help="Show the topologies help text including the layout.")
def show() -> None:
    help(topo_cls)


@cli.command(help="Do a small evaluation.")
@click.option("--rounds", type=click.INT, required=True,
              help="Amount of independent rounds to perform.")
@click.option("--count", type=click.INT, required=True,
              help="Amount of pings per round.")
def evaluation(rounds: int, count: int) -> None:
    pattern = re.compile("packet count: ([0-9]+)")

    col_width = 9

    def one_evaluation(csv_file, args, title):

        with topo_cls() as topo:

            header = list(topo.ns_names)
            header.insert(0, "# round")

            csv_line = "; ".join(word.ljust(col_width) for word in header)

        with open(csv_file, "a") as cf:
            cf.write(csv_line + "\n")

        for i in range(rounds):
            info("Do round " + str(i))

            with topo_cls() as topo:
                topo.log_dir = path.join(script_dir,
                                         title + "-eval{:02d}".format(i))
                debug("change log dir to " + str(topo.log_dir))

                topo.bin_args = args
                debug("change args to " + str(topo.bin_args))

                topo.start()
                ns_name = topo.ns_names[0]

                ping_cmd = ['ping', '-c', str(count),
                            topo.network[topo.ns_len].strNormal(0)]
                info("run command (ns: " + ns_name + ") " + " ".join(ping_cmd))
                nsp = NSPopen(ns_name, ping_cmd, stdout=sp.DEVNULL)

                debug("wait for process finish")
                nsp.wait()
                nsp.release()

                topo.stop()

                debug("wait for topology stop")
                topo.wait()

                csv_line_list = [str(i)]

                for ns_name in topo.ns_names:
                    ns = topo.ns[ns_name]
                    log_file = path.join(topo.log_dir, ns_name + ".log")
                    debug("access log file " + str(log_file))

                    match = None

                    with open(log_file) as lf:
                        for line in lf:
                            out = pattern.search(line)
                            # debug(str(bool(out)) + "---" + str(out))
                            if out:
                                match = out

                    info("{} sent {} packages during this round".format(
                        ns_name, str(match.group(1))))
                    csv_line_list.append(match.group(1))

                csv_line = "; ".join(
                    word.ljust(col_width) for word in csv_line_list)

                with open(csv_file, "a") as cf:
                    cf.write(csv_line + "\n")

    csv_ccack_file = path.join(script_dir, "evaluation_ccack.csv")

    if os.path.isfile(csv_ccack_file):
        error("rescue and delete your already existing results. File {} "
              "already exists.".format(csv_ccack_file))
        exit(1)

    csv_ralqe_file = path.join(script_dir, "evaluation_ralqe.csv")

    if os.path.isfile(csv_ralqe_file):
        error("rescue and delete your already existing results. File {} "
              "already exists.".format(csv_ralqe_file))
        exit(1)

    info("----- Do ccack evaluation -----")

    # TODO fetch options from class probably
    one_evaluation(csv_ccack_file, ["--ccack", "--gensize", "16"], "ccack")

    info("----- Do ralqe evaluation -----")

    one_evaluation(csv_ralqe_file, ["--gensize", "16"], "ralqe")


if __name__ == '__main__':
    cli()
