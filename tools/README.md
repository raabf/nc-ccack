Setup virtual environment
=========================

the `setup_venv.py` script allows to automatically setup an virtual wireless 
network for testing and developing purposes.

Installation and requirements
------------

 + A Linux kernel. Tested with debian kernel 4.8.8.
 + python >= 3.5
 + `pip3 install -r requirements.txt`
 + install [bcopeland's wmediumd](https://github.com/bcopeland/wmediumd)
    - apt install libevent-dev libconfig-dev
 + The `mac80211_hwsim` kernel module. It is part of the standard linux 
 kernel and therefore pre-installed. But note that some Linux distribution 
 kernel maintainer have removed the `mac80211_hwsim` module like it is the 
 case in Arch linux. It is available in Debian/Ubuntu.

Quick start
-----------

First decide on a topology. You can see a list via `setup_venv.py --help`. 
The simplest one is `Tri`, which is just a streight line of three nodes. You 
can show topology information as well as their layout via:

    setup_venv.py --topology=Tri show

Then run it with (you normally want to show `info` details in the console 
output):

    setup_venv.py --topology=Tri --verbosity=info run

The ouput of `wmediumd` and the individual logs can be found in the `./log/` 
subdirectory.

You can now start to use the network in the network namespaces (namespaces 
and log files are named after the corresponding node name), like:

    ip netns exec src ping -c15 10.0.1.3


At least have fun with the simple setup of topologies. Do not forget to run 
`clean` after you are finished with one topology.
