% !TeX spellcheck = en_GB
\documentclass[a4paper]{article}
\usepackage{xcolor}
\usepackage{colorspace}
\usepackage{amsmath}
\usepackage{fancyhdr, graphicx}
\usepackage{enumitem}
\usepackage{url}
\usepackage{hyperref}
\usepackage{graphicx}
\usepackage{todonotes}
\usepackage[margin=3cm]{geometry}
\usepackage[backend=bibtex, style=numeric]{biblatex}
\bibliography{library} 

%%% TUM Colours %%%

% Primary
\definecolor{tum-blue}{RGB}{0,101,189}
\definecolor{tum-white}{RGB}{255,255,255}
\definecolor{tum-black}{RGB}{0,0,0}

% Secondary

\definecolor{tum-blue-darker}{RGB}{0,82,147}
\definecolor{tum-blue-darkest}{RGB}{0,51,89}

% three gray colours 80%, 50%, and 20%
\definecolor{tum-gray80}{RGB}{088,088,090}
\definecolor{tum-gray50}{RGB}{156,157,159}
\definecolor{tum-gray20}{RGB}{217,218,219}

% accent

\definecolor{tum-accent-gray}{RGB}{218,215,203}
\definecolor{tum-accent-orange}{RGB}{227,114,34}
\definecolor{tum-accent-green}{RGB}{162,173,0}
\definecolor{tum-accent-blue-light}{RGB}{152,198,234}
\definecolor{tum-accent-blue-dark}{RGB}{100,160,200}

\renewcommand{\headrulewidth}{0pt}


\fancyhead[L]{\color{tum-blue}
  Chair for Network Architectures and Services\\
  Department of Informatics\\
  Technical University of Munich
}
\fancyhead[R]{
  \includegraphics[height=1.3cm]{tum-logo.pdf}
}

\title{Network Coding: Project Proposal\\
  \large Implementation of Cumulative Coded Acknowledgements (CCACK)}
\date{Winter Semester 16/17 \textbullet\space 10. January 2017}
\author{
  Fabian Raab\\
  \href{mailto:Fabian Raab <fabian.raab@tum.de>}{fabian.raab@tum.de}
  \and
  Jait Dixit\\
  \href{mailto:Jait Dixit <jait.dixit@tum.de>}{jait.dixit@tum.de}
}



\begin{document}

\maketitle
\thispagestyle{fancy}

\begin{flushleft}
  % bla
\end{flushleft}

\section*{Introduction}
\label{Introduction}

We will implement the \emph{Cumulative Coded Acknowledgement} (CCACK)\cite{ccack}, an Opportunistic Routing Protocol with feedback vectors, for the \verb|moep80211ncm| library. \verb|moep80211ncm| currently uses the classical method to estimate the amount of packets that each node forwards using the link loss rate. CCACK uses constant size feedback vectors to stop transmitting exactly at the time point when the neighbours have received all packets. Furthermore, the feedback vector contains information which we use to implement a more precise rate control algorithm. An evaluation\cite{ccack} has shown that CCACK performs better compared to MORE\cite{more} with up to 20x for throughput and 124\% for fairness.
This proposal includes why this project critically improves the library, which data structures have to be implemented, steps needed to implement our project and a rough time plan.

\section*{Relevance}

\verb|moep80211ncm| provides basic functions for \emph{Network Coding} including  \emph{Galois fields} of different sizes as well as functions to operate on them. The implementation provides bi-directional sessions between a source and a destination along a fixed path. It uses intra-session network coding to code packets belonging to a session. The library provides a \emph{Rate-adaptive link quality estimator} (RALQ) for determining the expected loss rate on a link. Based on this rate and the ETX metric, the algorithm calculates how many packets have to be sent so that the destination will be able to decode all packets with high probability. This seems attractive since it has minimal coordination overhead, but has the big disadvantage that this is just a guess and the precision highly depends on the quality of the link estimator. Furthermore, this method has significant performance drops in dynamic wireless networks with continuously changing levels of channel gains, interference, and background traffic.\cite{ccack}

We will solve this problem through implementing a feedback vector that each node broadcasts to its neighbours. Instead of predicting -- via link estimators -- the amount of packets to be sent, the nodes can just keep broadcasting packets until they know through the feedback that all of their neighbours have received all packets. To achieve that, we will implement the \emph{Cumulative Coded Acknowledgement} (CCACK) \cite{ccack} proposed by \citeauthor{ccack}. The CCACK approach includes such a feedback vector which has constant overhead. Additionally, it provides a rate control algorithm, which makes use of the information in the feedback vector. \citeauthor{ccack} showed that CCACK improves both the throughput and fairness of the transmission. Compared to MORE \cite{more}, a different MAC-independent Opportunistic Routing protocol for Network Coding, CCACK provides up to 20x more throughput and 124\% more fairness and on average 45\% and 8.8\%, respectively. Hence, our implementation of CCACK will make \verb|moep80211ncm| much more efficient and reliable.


%bla CCACK \cite{ccack} and C3ACK \cite{c3ack}

%\todo[inline]{
%  The project proposal (~2 A4 pages) is due until January 11, 2017. Please
%  commit it as PDF into your team Git. The proposal should describe
%
%  - what your project is all about,
%
%  - why your project is a contribution to network coding,
%
%  - how you plan to implement it (or proceed otherwise in case your
%  project does not contain an implementation part),
%
%  - a rough time plan until the end of the project phase [1], and
%
%  - how the work will be distributed to your team's members.
%
%  Please note that the proposal is already part of your project, i.e., it
%  will be considered for grading.
%}


\section*{Implementation}
We will first talk about additional data structures that will be needed to
implement CCACK. Then we look at the steps that we plan to undertake
for the implementation and lastly the challenges that we think we will face during
the implementation. Fabian will be responsible for implementing the data
structures and Jait will focus on the implementation. If a task requires work on
both the data structure and the implementation concurrently, we will work
together. The additional data structures that we require are:

\begin{itemize}
\item Buffers \(B_v, B_u, B_w\) for storing innovative coding vectors, coding
  vectors from upstream and broadcasted coding vectors respectively. This
  information will be maintained per flow.
\item Each element in \(B_u\) also has an associated value
  \texttt{usage\_count} which is used to generate the ACK coding vector. The
  algorithm requires the coding vectors with the lowest \texttt{usage\_count}.
  This will be made available via a min-heap.
\item \(M\) different \(N \times N\) hash matrices required by the algorithm to generate the ACK coding vector.
\item Additional information that must be stored per flow:
  \begin{itemize}
  \item flow information (can be extracted from the \texttt{session}
    sub-module in the current NCM implementation)
  \item credit counter
  \item differential backlog
  \item relative differential backlog
  \end{itemize}
\end{itemize}



The following lists the steps that we use as guide to implement our project:
\begin{enumerate}
\item Setup a simulation so that we can iterate on the code quickly.
\item Add CCACK and its parameters as command-line options for the NCM module.
\item Inject CCACK header using MOEP header extension mechanism. The header
  currently consists of two fields -- ACK coding vector and total differential backlog.
\item Modify the handlers for tap and radio interfaces in the NCM module to
  handle CCACK. The additional steps that we perform within these subroutines are:
  \begin{itemize}
  \item When a packet is received/overheard, then update the buffers \(B_u, B_v\) and also
    update counters for the flows.
  \item If a received/overheard packet contains CCACK header, we extract the
    ACK coding vector and check whether a downstream node already has
    received a packet coded with coding vectors from either \(B_u\) or \(B_w\).
  \item When broadcasting a coded packet, add the coding vector to \(B_w\). Also, an
    ACK coding vector is generated using the coding vectors in the \(B_u\) buffer.
  \end{itemize}
\item A credit counter based system is used for rate control. It is based on
  ``cumulative differential backlog'' for a given flow, at every node with
  respect to all the downstream nodes for that flow. A node will choose to
  broadcast packet for a given flow based on the counter value.
\end{enumerate}

We plan on using the C11 standard for our code following the Linux kernel coding
style guideline. Doxygen will be used as documentation style and for generating documentation for the project.
The challenges which we will face during the implementation are
mentioned below:

\begin{itemize}
\item Understanding how the NCM module currently works since the documentation
  is very sparse.
\item Implementing linear algebra operations:
  \begin{itemize}
  \item Solution(s) for a linear system
  \item Matrix-vector operations
  \item Row-based Gaussian elimination to maintain row-echelon form
  \item Check for linear independence
  \item Finding rank/dimension of a matrix
  \end{itemize}
\end{itemize}

\section*{Schedule}
% Timetable
\begin{center}
  \includegraphics[width=\textwidth]{schedule}
\end{center}

\printbibliography

\end{document}
