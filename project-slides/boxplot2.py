#!/usr/bin/python3

"""
=================================
Box plots with custom fill colors
=================================

This plot illustrates how to create two types of box plots
(rectangular and notched), and how to fill them with custom
colors by accessing the properties of the artists of the
box plots. Additionally, the ``labels`` parameter is used to
provide x-tick labels for each sample.

A good general reference on boxplots and their history can be found
here: http://vita.had.co.nz/papers/boxplots.pdf
"""

import matplotlib.pyplot as plt
import numpy as np

# Random test data
np.random.seed(123)
all_data = [np.random.normal(0, std, 100) for std in range(1, 4)]

fig, ax = plt.subplots(figsize=(6, 4))

width = 0.2
gap = 0.1

ind = np.arange(3)

adjust = (width + gap)+0.5

# rectangular box plot
bplot1 = ax.boxplot(all_data,
                         vert=True,   # vertical box aligmnent
                         patch_artist=True,   # fill with color
                         positions=ind+width/2+gap/2-adjust,
                         widths=width)

# rectangular box plot
bplot2 = ax.boxplot(all_data,
                         vert=True,   # vertical box aligmnent
                         patch_artist=True,   # fill with color
                         positions=ind-width/2-gap/2-adjust,
                         widths=width)

# fill with colors
colors = ['pink', 'lightblue', 'lightgreen']
for patch in bplot1['boxes']:
    patch.set_facecolor('lightgreen')

for patch in bplot2['boxes']:
    patch.set_facecolor('lightblue')

# adding horizontal grid lines
ax.yaxis.grid(True)
ax.set_xticks(ind-1+width/2+gap)
ax.set_xlabel('xlabel')
ax.set_ylabel('ylabel')

# # add x-tick labels
# plt.setp(ax, xticks=[y+1 for y in range(len(all_data))],
#          xticklabels=['x1', 'x2', 'x3'])

# fig.tight_layout()

plt.show()