/**
 * @file ccack_types.h
 * @author Fabian Raab <fabian.raab@tum.de>
 * @author Jait Dixit <jait.dixit@tum.de>
 * @date 12 March 2017
 * @brief Cumulative Coded Acknowledgements types header
 * @details Globally needed types.
 */

#ifndef __CCACK_TYPES_H_
#define __CCACK_TYPES_H_

struct ccack_hash_matrices;
typedef struct ccack_hash_matrices* ccack_hash_matrices_t;

struct ccack_neighbour;

struct ccack_block;
typedef struct ccack_block* ccack_block_t;

#endif
