/**
 * @file ccack.h
 * @author Fabian Raab <fabian.raab@tum.de>
 * @author Jait Dixit <jait.dixit@tum.de>
 * @date 12 March 2017
 * @brief Cumulative Coded Acknowledgements header
 * @details See ccack.c
 */

#ifndef CCACK_H_
#define CCACK_H_

#include <moep80211/dev.h>
#include <moep80211/moep_hdr_ext.h>

#include "params.h"
#include "frametypes.h"

// First exponential moving average parameter. Sum of them must be 1.
#define TOT_DB_GAMMA_DEFAULT 0.5
// Second exponential moving average parameter. Sum of them must be 1.
#define TOT_DB_DELTA_DEFAULT 0.5

int
ccack_setup(struct params_session *s_params, const uint8_t *hwaddr);

void
ccack_cleanup(struct ccack *cc);

unsigned int
ccack_get_db(const generation_t gen);

float
ccack_get_rel_db(const generation_t gen);

void
ccack_update_tot_db(struct params_session *s_params, float ctot_db);

void
ccack_inc_credit_db(const struct params_session s_params, struct ccack_block *cb);

void
ccack_dec_credit_db(struct ccack_block *cb);

ccack_block_t
ccack_block_init(const struct params_session *s_params);

int
ccack_buffer_tx_add(struct ccack_block *cb, const u8 *payload);

int
ccack_buffer_rx_add(struct ccack_block *cb, const u8 *payload);

void
ccack_block_cleanup(struct ccack_block *cb);

int
ccack_block_reset(struct ccack_block *cb);

int
ccack_init_ncm_hdr(const session_t s, const u8 sid[2 * IEEE80211_ALEN], struct ncm_hdr_ccack *hdr);

void
ccack_process_acv(session_t s, const u8 *local_addr, const u8 *remote_addr, u32 ctot_db, const u8 *acv);

int
ccack_block_master_rlnc_rank(struct ccack_block *cb);

int
ccack_block_slave_rlnc_rank(struct ccack_block *cb);

struct itimerspec *
ccack_rtx_timeout(const generation_t g);
#endif // CCACK_H_
