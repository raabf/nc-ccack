#ifndef __BCAST_H
#define __BCAST_H

#include <moep80211/types.h>

int bcast_add(u32 id);

int bcast_known(u32 id);

#endif //__BCAST_H
