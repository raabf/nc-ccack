#ifndef __FRAMETYPES_H
#define __FRAMETYPES_H

#include <moep80211/system.h>
#include <moep80211/types.h>
#include <moep80211/ieee80211_addr.h>

#include "generation.h"
#include "global.h"

enum headertypes {
	NCM_HDR_DATA	= MOEP_HDR_VENDOR_MIN,
	NCM_HDR_CODED,
	NCM_HDR_BCAST,
	NCM_HDR_BEACON,
	/*!
	 * CCACK header type used in ccack.c
	 */
	NCM_HDR_CCACK,
	NCM_HDR_INVALID	= MOEP_HDR_COUNT - 1
};

enum frametypes {
	NCM_DATA = 0,
	NCM_CODED,
	NCM_BEACON,
	/*!
	 * only required if you send an struct ::ncm_hdr_ccack alone, which can be theoretically piggybacked with
	 * every packet.
	 */
	NCM_CCACK,
	NCM_INVALID,
};

struct ncm_hdr_beacon {
	struct moep_hdr_ext hdr;
} __attribute__((packed));

struct ncm_beacon_payload {
	u8 mac[IEEE80211_ALEN];
	u16 p;
	u16 q;
} __attribute__((packed));

/**
 * Bware: the coding header is a variable-length header, depending on the galois
 * field and generation size.
 */
struct ncm_hdr_coded {
	struct moep_hdr_ext hdr;
	u8 sid[2 * IEEE80211_ALEN];
	// Type of the galois filed (MOEPGF_TYPE).
	u8 gf: 2;
	u8 window_size: 6;
	u16 seq;
	u16 lseq;
	struct generation_feedback fb[0];
} __attribute__((packed));

#if GENERATION_MAX_SIZE > 255
#error dimension does not fit anymore into the type of struct ncm_hdr_ccack:ctot_db
#endif

/*! @struct ncm_hdr_ccack
 * Header struct of the CCACK module in #ccack.c .
 * Is usually used together with the `struct ncm_hdr_coded`, but is not strictly necessary since ccack ACK vectors can
 * be sent without coded data. However you need to know the struct ncm_hdr_coded::gf and the generation size for the
 * `ack` vector which has varibale length.
 *
 * @see ccack_init_ncm_hdr()
 */
struct ncm_hdr_ccack {
	/*!
	 * Moep header management variable
	 */
	struct moep_hdr_ext hdr;

	/*!
	 * @brief Session identifier to which the acknowledgement belongs to.
	 *
	 * Has the same format and meaning as struct ncm_hdr_coded::sid . Is usually sent with
	 * struct ::ncm_hdr_coded and is equal to struct ncm_hdr_coded::sid, but this is not strictly necessary.
	 */
	u8 sid[2 * IEEE80211_ALEN];

	// current total differential backlog
	/*!
	 * @brief Current total differential backlog
	 *
	 * Current total differential backlog of the node indicating how much backpressure caused trough active sessions
	 * the node has.
	 *
	 * @see session_get_ccack_ctot_db()
	 */
	uint32_t ctot_db;

	/**
	 * @brief Compressed ACK coding vector with variable length.
	 *
	 * Elements have the size of struct moepgf:exponent and the
	 * amount of elements is equal to the generation size from struct params_session::gensize. For detemining the
	 * correct `struct moepgf`, struct ncm_hdr_coded:gf is required.
	 *
	 * @see ccack_process_acv()
	 * @see generate_acv()
	 */
	u8 acv[0];
} __attribute__((packed));

struct ncm_hdr_bcast {
	struct moep_hdr_ext hdr;
	u32 id;
} __attribute__((packed));

#endif //__FRAMETYPES_H
