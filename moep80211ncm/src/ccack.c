/**
 * @file ccack.c
 * @author Fabian Raab <fabian.raab@tum.de>
 * @author Jait Dixit <jait.dixit@tum.de>
 * @date 12 March 2017
 * @brief Cumulative Coded Acknowledgements
 * @details
 *
 * Instead of measuring the link quality of the arcs and estimating the required packets to send, this approach uses
 * node to node acknowledgements added to every transmission, so that all surrounding nodes can overhear that. With
 * the ack the nodes know many linear independent packets the surrounding nodes have.
 *
 * The approach of this file is based on the paper:
 * Koutsonikolas, D., C. C. Wang, und Y. C. Hu.
 * „Efficient Network-Coding-Based Opportunistic Routing Through Cumulative Coded Acknowledgments“.
 * IEEE/ACM Transactions on Networking 19, Nr. 5 (Oktober 2011): 1368–81. doi:10.1109/TNET.2011.2111382.
 *
 * Compared to the paper our approach and algorithms are a little bit modified.
 * The paper describes the approach for an unidirectional transmission. This version here is modified for a
 * bidirectional transmission called session here. Nodes keep watching the ranks of their neighbours in both directions
 * (master and slave) instead of just along a single flow.
 * The source code does have some comments on how the approach is different compared to the paper.
 *
 * Terms:
 * -----
 * *innovative:*  A packet can be innovative to a node if it is linear independent to the already existing packets of
 * 	that node
 *
 * *compressed vector:* A vector contains multiple galois field (gf) elements. The bit size of one gf element is
 * equal to the gf exponent. A vector is compressed if every element needs exactly gf exponent bits in storage.
 * Therefore, multiple gf elements can be in one byte. The memory area is always padded to a full byte, but the
 * generation site should be anyway an exponent of two. Use only get_gf_element() and set_gf_element() to access
 * specific elements in the vector.
 *
 * *uncompressed vector:*  A vector is uncompressed, if one gf element uses one byte of storage regardless of its real
 * size. Access elements just via subscripts.
 *
 * Status:
 * ------
 * The ACK coding vector is correctly generated, transmitted and the hash tests are correctly performed. The rank of
 * the local generation state, the master heard rank and the slave heard rank are growing correctly and are sometimes
 * equal in between and at a full generation. The sending of new packets is stopped via the cancelling of the rtx timer,
 * if there is a new innovative packet we reset the rtx timer, and ack are sent by master and slave regularly when
 * the sending of data packets is stopped.
 *
 * @todo Completely deactivate the RALQE and the corresponding file neighbour.c . Includes stop sending beacons,
 * measuring the link quality and adapting the rtx timer to the current loss estimation. Currently most things are
 * calculated, but are not applied at the end (because they are replaced by ccack).
 * @todo there is still a nasty bug which freezes completely randomly (possibly after thousands of packets) some nodes.
 * It gets better with dirty hack when we add a counter in cb_ack() which resets the rtx timer after some amount of
 * consecutively called cb_ack() functions.
 * @todo probably more testing. Our tests are primarily based on a line network with a master and a slave and a single
 * forwarder in between which can reach master and slave.
 */

#include <string.h>
#include <limits.h>
#include <moepcommon/types.h>
#include <moepcommon/list.h>
#include <moepcommon/timeout.h>
#include <moeprlnc/rlnc.h>
#include <moepgf/moepgf.h>
#include <moeprlnc/rlnc.h>

#include "ccack.h"
#include "session.h"
#include "../libmoepcommon/include/moepcommon/list.h"

/*! \struct ccack_hash_matrices
 * @brief This structure stores a node's hash matrices
 *
 * Hash matrices as the name suggests are used to hash coding vectors (generated locally or received from neighbours).
 * For performance reasons, these matrices are randomly generated diagonal matrices. Each matrix contains 'gensize'
 * elements and occupies the same space as a compressed coding vector. A node's hardware address is used as a seed for
 * the RNG. This is due to the fact that a node needs to generate a remote node's hash matrices to determine its coding
 * sub-space.
 *
 */
struct ccack_hash_matrices {
	/*! Pointer to pointers which point to the matrices */
	uint8_t         **array;
	/*! Random seed used to generate the matrices */
	unsigned int    r_seed;
};

/*! \struct ccack_neighbour
 * @brief This structure functions as a cache for a node's hash matrices
 *
 * Hash matrices for a node are generated only once for the duration of the program. Each time a session needs a
 * node's hash matrices, first a list of ccack_neighbour objects is checked.
 */
struct ccack_neighbour {
	/*! list_head to allow this struct to be inserted into a linked list */
	struct list_head list;
	/*! node's hardware address */
	u8 hwaddr[IEEE80211_ALEN];
	/*! Hash matrices */
	struct ccack_hash_matrices *hm;
};

/*! \struct cv_buffer_entry
 * @brief This structure is what is stored inside ccack_block::buffer_rx or ccack_block::buffer_tx
 *
 * #flags store the direction which has acknowledged a coding vector. It can takes one or both of #DIR_TOWARDS_MASTER
 * or #DIR_TOWARDS_SLAVE.
 */
struct cv_buffer_entry {
	/*! list_head to allow this struct to be inserted into a linked list */
	struct list_head list;
	/*! counter used to keep track of how many times this coding vector was used in generating an acv */
	unsigned int counter;
	/*! stores the direction which acknowledged this coding vector */
	uint8_t flags;
	/*! Zero-length array to store the actual coding vector */
	uint8_t cv[0];
};

/*! \struct ccack_block
 * @brief This structure stores all CCACK related information
 *
 * We store pointer to the recevied and transmitted coding vectors that are received during a session. Since, a ccack_block
 * is unique to a session. #rb_master and #rb_slave are used to keep track of rank of coding vectors acknowledged from
 * direction towards master slave respectively.
 *
 */
struct ccack_block {
	/*! \struct buffer_tx
	 * @brief This structure wraps a list of coding vectors that a node transmits
	 */
	struct buffer_tx {
		/*! list_head to allow this struct to be inserted into a linked list*/
		struct list_head b;
		/*! number of coding vectors in this buffer */
		unsigned int	 count;
	} buffer_tx;

	/*! \struct buffer_rx
	 * @brief This structure wraps a list of coding vectors that a node has received
	 */
	struct buffer_rx {
		/*! list_head to allow this struct to be inserted into a linked list*/
		struct list_head b;
		/*! number of coding vectors in this buffer */
		unsigned int 	 count;
	} buffer_rx;

	/*! Store linearly independent coding vectors acknowledged by nodes closer to master */
	rlnc_block_t rb_master;
	/*! Store linearly independent coding vectors acknowledged by nodes closer to slave */
	rlnc_block_t rb_slave;

	/*! struct moepgf for easy access to galois field arithmetic functions */
	struct moepgf gf;

	/*!
	 * credit counter used to determine time-out for packets in a generation
	 *
	 * @see inc_credit_db()
	 * @see dec_credit_db()
	 */
	float credit_db;

	/*! length in bytes of a compressed coding vector */
	size_t cv_len;
};

/**
 * @defgroup gen_acv ACV generation
 * A group of functions which generates a new Acknowledgement Coding Vector.
 */

/**
 * @defgroup gf_manipulation Compressed gf vectors manipulation
 * Allows to set/get and compute with elements in a compressed Galois Field elements vector.
 */

/**
 * @defgroup db Differential Backlog
 * Operations on different Backlog.
 */

/**
 * @defgroup buffers CCACK buffers
 * Operations in CCACK buffers and datastructures.
 */

/**
 * @brief Returns the \a idx 'th element of an compressed gf vector in \a mem.
 *
 * @ingroup gf_manipulation
 * @param mem The begin of some memory location where the vector is stored.
 * @param gftype The gf type of the vector.
 * @param idx The index of the requested element.
 * @return One gf element in one byte from \a idx position.
 */
static inline uint8_t
get_gf_element(const uint8_t *mem, enum MOEPGF_TYPE gftype, int idx)
{
	int byte = (idx * moepgf_gf_exponent(gftype)) / 8;
	int pos  = (idx * moepgf_gf_exponent(gftype)) % 8;

	return (mem[byte] >> pos) & moepgf_gf_mask(gftype);
}

/**
 * @brief Sets the \a idx 'th element of an compressed gf vector in \a mem.
 *
 * @ingroup gf_manipulation
 * @param mem The begin of some memory location where the vector is stored.
 * @param gftype The gf type of the vector.
 * @param idx The index of the requested element.
 * @param val The new value of the \a idx 'th element.
 */
static inline void
set_gf_element(uint8_t *mem, enum MOEPGF_TYPE gftype, int idx, uint8_t val)
{
	int byte = (idx * moepgf_gf_exponent(gftype)) / 8;
	int pos  = (idx * moepgf_gf_exponent(gftype)) % 8;

	mem[byte] &= ~(moepgf_gf_mask(gftype) << pos);
	mem[byte] |= (val & moepgf_gf_mask(gftype)) << pos;
}

/**
 * @brief Calculate differential backlog for a generation
 *
 * @ingroup db
 * @param gen Pointer to a generation
 * @return Differential backlog
 * @see ccack_rtx_timeout()
 */
unsigned int
ccack_get_db(const generation_t gen)
{
	unsigned int gen_rank = 0;
	gen_rank += generation_encoder_dimension(gen);
	gen_rank += generation_decoder_dimension(gen);

	session_t s = generation_get_session(gen);
	struct ccack_block *cb = session_get_ccack_block(s);

	int db_master = gen_rank - rlnc_block_rank_decode(cb->rb_master);
	int db_slave = gen_rank - rlnc_block_rank_decode(cb->rb_slave);

	// max(0, min(db_master, db_slave))
	int min_rank = db_master < db_slave ? db_master : db_slave;
	return min_rank > 0 ? min_rank : 0;
}

/**
 * @brief Calculate relative differential backlog for a generation
 *
 * @ingroup db
 * @param gen Pointer to a generation
 * @return Relative differential backlog
 * @see ccack_rtx_timeout()
 */
float
ccack_get_rel_db(const generation_t gen)
{
	session_t s = generation_get_session(gen);
	struct params_session *params_s = session_get_params_session(s);

	float db = ccack_get_db(gen) * 1.0;

	return  db / (db + params_s->ccack.tot_db);
}

/**
 * @brief Update the total differential backlog locally.
 *
 * The total differential backlog is maintained as an exponential
 * moving average of the current total differential backlog (\f$\Delta Q^{tot}_{n_j}\f$) that a node sends as
 * a part of its CCACK header.
 *
 * The weights for exponential moving average are set using #TOT_DB_GAMMA_DEFAULT and #TOT_DB_DELTA_DEFAULT. The
 * default for both the values are 0.5.
 *
 * @ingroup db
 * @param gen Pointer to a generation
 * @return Relative differential backlog
 * @see ccack_rtx_timeout()
 */
static void
update_tot_db(struct params_session *s_params, u32 ctot_db)
{
	s_params->ccack.tot_db = TOT_DB_GAMMA_DEFAULT * s_params->ccack.tot_db + TOT_DB_DELTA_DEFAULT * ctot_db;
}

/**
 * @brief Update credit for a ccack_block
 *
 * This is a stub implementation for future use.
 *
 * @ingroup db
 * @param s_params Session parameters
 * @param cb Pointer to a ccack_block
 */
static void
inc_credit_db(const struct params_session *s_params, struct ccack_block *cb)
{
	cb->credit_db = s_params->ccack.alpha * cb->credit_db + s_params->ccack.beta;
}

/**
 * @brief Decrement credit for a ccack_block
 *
 * This is a stub implementation for future use.
 *
 * @ingroup db
 * @param cb Pointer to a ccack_block
 */
void
ccack_dec_credit_db(struct ccack_block *cb)
{
	cb->credit_db -= 1.0;
}

/**
 * @brief Sorts a cv_buffer list via reversed insertion sort.
 *
 * Since in every generation of an ack coding vector just some elements are increased by +1 in a beforehand sorted
 * list, this function should run nearly in O(n) time.
 *
 * @ingroup gen_acv
 * @param head The head of the cv_buffer list.
 */
static void
cv_buffer_special_sort(struct list_head *head)
{
	if (list_is_singular(head)) {
		return;
	}

	struct cv_buffer_entry *cur1;
	struct cv_buffer_entry *prev1;
	struct cv_buffer_entry *next1;

	struct cv_buffer_entry *cur2;
	struct cv_buffer_entry *next2;

	struct list_head *insert_before = head;

	list_for_each_entry_safe_reverse(cur1, prev1, head, list) {

		if (cur1->list.next != head) {
			next1 = list_first_entry(&cur1->list, struct cv_buffer_entry, list);
			/*
			 * This check is only for efficiency reasons. It will jump over elements already in order, it prevents:
			 *  - That an element is deleted and re-inserted in the same position.
			 *  - That the order of consecutive elements with the same count will be inverted (cause of the < later)
			 */
			if (cur1->counter <= next1->counter)
				continue;
		}

		cur2 = list_entry(cur1->list.next, typeof(*cur1), list);
		list_del(&cur1->list);

		insert_before = head;

		list_for_each_entry_safe_from(cur2, next2, head, list) {
			/*
			 * < is important. NOT <= here! Or elements wich are -1 small after an element which is == can be look over.
			 * And it will result in an infinite loop.
			 */
			if (cur1->counter < cur2->counter) {
				insert_before = &cur2->list;
				break;
			}
		}
		list_add_tail(&cur1->list, insert_before);
	}
}



/**
 * @brief Increments \a cvbe and sorts it in the list.
 *
 * @attention Do not use this function during iteration of \a head since it moves elements forward and this can result
 * in an infinite loop.
 *
 * @ingroup gen_acv
 * @param cvbe The coefficient vector buffer entry which should be incremented and sorted.
 * @param head The head of the cv_buffer list.
 */
static void
inc_counter_cv_buffer_entry(struct cv_buffer_entry *cvbe, struct list_head *head)
{
	cvbe->counter++;

	LOG(LOG_ERR, "inc to %d of %s", cvbe->counter, hexdump2(&cvbe->cv[0], 16));

//	struct list_head *next = cvbe->list->next;
//	struct cv_buffer_entry *cur = list_last_entry(&cvbe->list, typeof(cvbe), list);

	struct cv_buffer_entry *cur;
	struct cv_buffer_entry *tmp;

	list_del(&cvbe->list);

	struct list_head *insert_before = head;

	list_for_each_entry_safe(cur, tmp, head, list) {
		if (cvbe->counter < cur->counter) {
			insert_before = &cur->list;
			break;
		}
	}

	list_add_tail(&(cvbe)->list, insert_before);
}

/**
 * @brief returns the minimum compared to #counter of the cb::buffer_rx list.
 *
 * @ingroup buffers
 * @deprecated cb::buffer_rx is now sorted, so just use the first element.
 * @param cb The ccack block.
 * @return The minimum entry.
 */
static struct cv_buffer_entry *
get_buffer_rx_min(struct ccack_block *cb)
{
	struct cv_buffer_entry *cur;
	unsigned int min_c = UINT_MAX;
	struct cv_buffer_entry *min_e = NULL;

	list_for_each_entry(cur, &cb->buffer_rx.b, list) {
		if (min_c >= cur->counter) {
			min_c = cur->counter;
			min_e = cur;
		}
	}

	return min_e;
}

/**
 * @brief Generate hash matrices for node using its hardware address
 *
 * @ingroup buffers
 * @param params_s  Session parameters
 * @param hwaddr Hardware address to be used as a seed for hash matrices
 * @return Pointer to struct ccack_hash_matrices
 */
static struct ccack_hash_matrices *
create_hash_matrices(struct params_session *params_s, const uint8_t *hwaddr)
{
	struct ccack_hash_matrices * hm;

	if (NULL == (hm = malloc(sizeof(struct ccack_hash_matrices)))) {
		LOG(LOG_ERR, "malloc() failed");
		return NULL;
	}

	memset(hm, 0, sizeof(*hm));

	memcpy(&hm->r_seed, hwaddr, IEEE80211_ALEN);

	// temporary vars
	int mask;

	if (0 > (mask = moepgf_gf_mask(params_s->gftype))) {
		LOG(LOG_ERR, "invalid gftype: %d", params_s->gftype);
		return NULL;
	}

	if (NULL == (hm->array = malloc(sizeof(*hm->array) * params_s->ccack.hash_matrices_num))) {
		LOG(LOG_ERR, "malloc() failed");
		return NULL;
	}

	for (int i = 0; i < params_s->ccack.hash_matrices_num; ++i) {
		if (NULL == (hm->array[i] = malloc(rlnc_block_cv_len(params_s->gftype, params_s->gensize) * params_s->gensize))) {
			LOG(LOG_ERR, "malloc() failed");
			return NULL;
		}

		for (int j = 0; j < params_s->gensize; ++j) {
			set_gf_element(hm->array[i], params_s->gftype, j, (uint8_t)rand_r(&hm->r_seed));
		}
	}

	return hm;
}

/**
 * @brief Do a hadamard product of two vectors.
 *
 * @ingroup gf_manipulation
 * @param dst Compressed gf destionation vector of \a size.
 * @param vec1 First compressed gf input vector of \a size.
 * @param vec2 Second compressed gf input vector of \a size.
 * @param size Size of the vectors.
 * @param gf gf strict for computations.
 */
static void
hadamard_vec_vec_prod(uint8_t *dst, uint8_t *vec1, uint8_t *vec2, size_t size, const struct moepgf *gf)
{
	int ret;
	uint8_t *elem1;
	if ((ret = posix_memalign((void *)&elem1, MOEPGF_MAX_ALIGNMENT, aligned_length(1, MOEPGF_MAX_ALIGNMENT)))) {
		DIE("[ccack] posix_memalign() failed with %d", ret);
		return;
	}
	memset(elem1, 0, aligned_length(1, MOEPGF_MAX_ALIGNMENT));

	// iterate element wise of the vectors
	for (int i = 0; i < size; ++i) {
		elem1[0] = get_gf_element(vec1, gf->type, i);
		uint8_t elem2 = get_gf_element(vec2, gf->type, i);
//		fprintf(stderr, "elem1 %s\n", hexdump2(elem1, 16));
//		fprintf(stderr, "&elem2 %s\n", hexdump2(&elem2, 1));
		gf->mulrc(elem1, elem2, 1);
		set_gf_element(dst, gf->type, i, elem1[0]);
	}

	free(elem1);
}

/**
 * @brief Do a dot product of two vectors.
 *
 * @ingroup gf_manipulation
 * @param vec1 First compressed gf input vector of \a size.
 * @param vec2 Second compressed gf input vector of \a size.
 * @param size Size of the vectors.
 * @param gf gf strict for computations.
 * @return The scalar result.
 */
static uint8_t
dot_prod(const uint8_t *vec1, const uint8_t *vec2, size_t size, const struct moepgf *gf)
{
	int ret;
	uint8_t *res, final;
	if ((ret = posix_memalign((void *)&res, MOEPGF_MAX_ALIGNMENT, aligned_length(1, MOEPGF_MAX_ALIGNMENT)))) {
		DIE("[ccack] posix_memalign() failed with %d", ret);
		return -1;
	}
	memset(res, 0, aligned_length(1, MOEPGF_MAX_ALIGNMENT));

	uint8_t *elem1;
	if ((ret = posix_memalign((void *)&elem1, MOEPGF_MAX_ALIGNMENT, aligned_length(1, MOEPGF_MAX_ALIGNMENT)))) {
		DIE("[ccack] posix_memalign() failed with %d", ret);
		return -1;
	}
	memset(elem1, 0, aligned_length(1, MOEPGF_MAX_ALIGNMENT));
	for (int j = 0; j < size; ++j) {
		elem1[0] = get_gf_element(vec1, gf->type, j);
		// multiply two elements and add it to res
		gf->maddrc(res, elem1, get_gf_element(vec2, gf->type, j), 1);
	}

	final = res[0];

	// cleanup
	free(res);
	free(elem1);

	return final;
}

/**
 * @brief Get a ccack_neighbour entry which matches the supplied hardware address.
 *
 * @ingroup buffers
 * @param head Head of list which is to be searched
 * @param addr Hardware address of a node to look for
 * @return  Pointer to ccack_neighbour object
 */
static struct ccack_neighbour *
find_neighbour(struct list_head *head, const u8 *addr)
{
	struct ccack_neighbour *ptr;

	list_for_each_entry(ptr, head, list) {
		if (0 == memcmp(&ptr->hwaddr[0], addr, IEEE80211_ALEN))
			return ptr;
	}

	return NULL;
}

/**
 * @brief Get hash matrices for a node
 *
 * Generate/fetch hash matrices for a node which has the supplied hardware address. The function first searches for the
 * node in the list and generates hash matrices if no entry is found.
 *
 * @ingroup buffers
 * @param params_s Session parameters
 * @param head Head of list to be searched
 * @param addr Hardware address of a node for which hash matrices are required
 * @return Pointer to ccack_hash_matrices object
 */
static struct ccack_hash_matrices *
get_neighbour_hash_matrices(struct params_session *params_s, struct list_head *head, const u8 *addr)
{
	struct ccack_neighbour *nb;

	if (NULL == (nb = find_neighbour(head, addr))) {
		if (NULL == (nb = malloc(sizeof(struct ccack_neighbour)))) {
			DIE("malloc() failed");
		}
		memcpy(&nb->hwaddr[0], addr, IEEE80211_ALEN);
		nb->hm = create_hash_matrices(params_s, addr);
		list_add(&nb->list, head);
		LOG(LOG_DEBUG, "[ccack] neighbour hash matrices generated for %s", hexdump2(addr, IEEE80211_ALEN));
	}

	return nb->hm;
}

/**
 * @brief Determine direction of acknowledgement
 *
 * This is a dumb implementation which works for simple linear topologies. It assumes that a remote node with a hardware
 * address smaller than the local address is towards master and node with a larger hardware address is towards the slave.
 *
 * @note This will be eventually replaced by a function from the OR module which returns the correct direction
 * based on the topology.
 * @param local_addr Hardware address of this node
 * @param remote_addr Hardware address of a remote node
 * @param sid Session ID for which the direction is required
 * @return Integer signifying direction, either #DIR_TOWARDS_MASTER or #DIR_TOWARDS_SLAVE
 */
static uint8_t
get_direction(const u8 *local_addr, const u8 *remote_addr, const u8 *sid)
{
	int ret = memcmp(local_addr, remote_addr, IEEE80211_ALEN);

	if (ret < 0) {
		return DIR_TOWARDS_SLAVE;
	} else {
		return DIR_TOWARDS_MASTER;
	}

	return 0;
}

/**
 * @brief Setup global ccack data structures in params_session.
 *
 * @param params_s Session parameters
 * @param hwaddr Nodes (local) hardware address
 * @return 0 if successful
 * @return -1 otherwise
 */
int
ccack_setup(struct params_session *params_s, const uint8_t *hwaddr)
{
	params_s->ccack.neighbours = malloc(sizeof(struct list_head));
	INIT_LIST_HEAD(params_s->ccack.neighbours);

	if (NULL == (params_s->ccack.hash_matrices_own = create_hash_matrices(params_s, hwaddr))) {
		return -1;
	}

	return 0;
}

/**
 * @brief Generate a new ccack_block
 *
 * @param params_s Session parameters
 * @return Pointer to a new ccack_block
 */
struct ccack_block *
ccack_block_init(const struct params_session *params_s)
{
	struct ccack_block *cb;

	if (NULL == (cb = malloc(sizeof(struct ccack_block)))) {
		LOG(LOG_ERR, "malloc() failed");
		return NULL;
	}

	cb->cv_len = rlnc_block_cv_len(params_s->gftype, params_s->gensize);
	cb->rb_master = rlnc_block_init(params_s->gensize, 0, MEMORY_ALIGNMENT, params_s->gftype);
	cb->rb_slave = rlnc_block_init(params_s->gensize, 0, MEMORY_ALIGNMENT, params_s->gftype);
	moepgf_init(&cb->gf, params_s->gftype, MOEPGF_ALGORITHM_BEST);

	INIT_LIST_HEAD(&cb->buffer_tx.b);
	cb->buffer_tx.count = 0;
	INIT_LIST_HEAD(&cb->buffer_rx.b);
	cb->buffer_rx.count = 0;

	return cb;
}

/**
 * @brief comparison function for heapsort
 *
 * Comparison function to be used when using Linux kernel's heapsort to sort cv_buffer_entrys in a list.
 * It compares using the buffer entry's cv_buffer_entry::count variable.
 *
 * @ingroup gen_acv
 * @param priv Additional data for comparison
 * @param a struct list_head of a cv_buffer_entry
 * @param b struct list_head of a cv_buffer_entry
 * @return 0 if equal
 * @return -1 if a < b
 * @return 1 if a > b
 */
static int
cmp_cv_buffer_counter(void *priv, struct list_head *a, struct list_head *b)
{

	struct cv_buffer_entry *ae = list_entry(a, typeof(*ae), list);
	struct cv_buffer_entry *be = list_entry(b, typeof(*be), list);

	if (ae->counter < be->counter) {
		return -1;
	}
	if (ae->counter == be->counter) {
		return 0;
	} else {
		return 1;
	}
}

/**
 * @brief Hashes coding vectors out of struct ccack_block::buffer_rx into a new \a hashed_cv_rb.
 *
 * It chooses those vectors out of struct ccack_block::buffer_rx (referenced in session_t::cb) which have the lowest
 * buffer_rx::count. Every time a rx cv is used,
 * his count is incremented. That should prevent that always the same and probably then not innovative cv are
 * acknowledged. It uses the own hash matrices to hash into a smaller acv.
 * It has to iterate either once trough struct ccack_block::buffer_rx or until some limit of linear independent cv is
 * reached. Worst case is therefore O(size struct ccack_block::buffer_rx).
 *
 * @ingroup gen_acv
 * @param s Corresponding session struct.
 * @param hashed_cv_rb An empty but allocated rlnc_block. The results will be written here.
 * @return == 0 on success
 * @return != 0 on failure
 */
int
generate_hashed_cv_rb(const session_t s, rlnc_block_t hashed_cv_rb)
{
	struct params_session *params_s = session_get_params_session(s);
	struct ccack_block *cb = session_get_ccack_block(s);

	// N - 1 - M
	unsigned int max_rank = params_s->gensize - 1 - params_s->ccack.hash_matrices_num;

	// should be impossible state since we create a session (and therefore ccack) only after receiving at least one.
	if (list_empty(&cb->buffer_rx.b)) {
		LOG(LOG_ERR, "[ccack] buffer neighbour empty during acv generation");
		return 0;
	}

	uint8_t **hm_own = params_s->ccack.hash_matrices_own->array; // own hash matrices

	struct cv_buffer_entry *cur;
	struct cv_buffer_entry *tmp;
	u8 *cur_hashed = malloc(cb->cv_len);
	memset(cur_hashed, 0, cb->cv_len);

	/*
	 * The algorithm is a little bit modified compared to the CCACK paper, since it has some flaws.
	 * The paper proposes to use a while loop:
	 *   while (hashed_cv_rb.rank <= params_s->gensize - 1 - params_s->ccack.hash_matrices_num) {...}
	 *
	 * However it could happen many times that the same vector is chosen from the buffer, but the same vector
	 * multiplied with the same hash matrix can never add new linear independent vectors. Therefore computation power
	 * is just wasted here.
	 * More important, if the hashed_cv_rb buffer is not big enough yet to add sufficient innovative vectors to the
	 * buffer, the while will end in a infinite loop.
	 *
	 * To solve that we select every hashed_cv_rb element just once and break the iteration in between if the while
	 * condition is fulfilled.
	 */
	list_for_each_entry_safe(cur, tmp, &cb->buffer_rx.b, list) {

		for (int j = 0; j < params_s->ccack.hash_matrices_num; ++j) {
//			fprintf(stderr, "cv %s\n", hexdump2(&cur->cv[0], cb->cv_len));
//			fprintf(stderr, "&hm_own[j][0] %s\n", hexdump2(&hm_own[j][0], cb->cv_len));
//			fprintf(stderr, "cur_hashed %s\n", hexdump2(cur_hashed, cb->cv_len));
			hadamard_vec_vec_prod(cur_hashed, &cur->cv[0], &hm_own[j][0], params_s->gensize, &cb->gf);

			if (0 != rlnc_block_decode(hashed_cv_rb, cur_hashed, cb->cv_len)) {
				LOG(LOG_ERR, "[ccack] rlnc_block_decode() failed");
				return -1;
			}


			if (rlnc_block_rank_decode(hashed_cv_rb) >= max_rank)
				break; // first level beak
		}

		// increment so that it is more unlikely that the same acv is acknowledged again.
		cur->counter++;

		// do not work, since we can not modify the list during we iterate trough it
//		inc_counter_cv_buffer_entry(cur, &cb->buffer_rx.b);

		if (rlnc_block_rank_decode(hashed_cv_rb) >= max_rank)
			break; // second level beak
	}

	free(cur_hashed);

	cv_buffer_special_sort(&cb->buffer_rx.b);

	// This approach (the original one) do not work because we do not check if cb->buffer_rx.b has enough elements.
	/*
	while (rlnc_block_rank_decode(hashed_cv_rb) <= max_rank) {

		// choose u from Bu
		struct cv_buffer_entry nb = cb->buffer_rx.b.next;

		for (int j = 0; j < params_s->ccack.hash_matrices_num; ++j) {

			u8 *nb_hashed = malloc(cb->cv_len);
			hadamard_vec_vec_prod(nb_hashed, nb, *hm_own[j], params_s->gensize, cb->gf);

			if (0 != rlnc_block_decode(hashed_cv_rb, nb_hashed, cb->cv_len)) {
				LOG(LOG_ERR, "[ccack] rlnc_block_decode() failed");
				return NULL;
			}

		}
		inc_counter_cv_buffer_entry(nb, cb);
	} */

	return 0;
}

/**
 * @brief Generates an Acknowledgement Coding Vector and stores it in \a acv.
 *
 * Solves the \f$ \mathbf{\Delta} \cdot \mathbf{c} = \mathbf{0} \f$ equation and return \f$\mathbf{c}\f$ in \a acv.
 * \a hashed_cv_rb must be in row-echelon form, the lower triangular must be 0, and the diagonal must be 1. However the
 * upper triangular can be zero as well as non zero. It has to iterate two times trough the rows and two times trough
 * the columns respectively, therefore worst case is I(generation size).
 *
 * @ingroup gen_acv
 * @param s Corresponding session struct.
 * @param hashed_cv_rb \f$\mathbf{\Delta}\f$ is the hashed coding vector matrix generated by #generate_hashed_cv_rb().
 * @param acv The result as a newly randomly generated Acknowledgement Coding Vector.
 * @return == 0 on success
 * @return != 0 on failure
 * @see generate_hashed_cv_rb()
 */
int
generate_acv_solve_eq(const session_t s, rlnc_block_t hashed_cv_rb, u8 *acv)
{
	struct params_session *params_s = session_get_params_session(s);
	struct ccack_block *cb = session_get_ccack_block(s);
	int ret;

	int rank = rlnc_block_rank_decode(hashed_cv_rb);
	LOG(LOG_DEBUG, "hashed cv matrix delta, cv_len: %d, rank: %d", (int) cb->cv_len, rank);

	uint8_t *hcv = malloc(cb->cv_len); // hashed coding vector
	memset(hcv, 0, cb->cv_len);  // not necessary since overwritten later

	for (int i = 0; i < params_s->gensize; ++i) {
		if (0 >= rlnc_block_get_cv(hashed_cv_rb, i, hcv, cb->cv_len)) {
			// set acv randomly to rand() & cb->gf.mask (for debugging also i is possible)
			set_gf_element(acv, params_s->gftype, i, rand() & cb->gf.mask);
		} else {
			// output for debugging
			fprintf(stderr, "[%02d]  ", i);
			hexdump(hcv, cb->cv_len);
		}
	}

	uint8_t *res;
	if ((ret = posix_memalign((void *)&res, MOEPGF_MAX_ALIGNMENT, aligned_length(1, MOEPGF_MAX_ALIGNMENT)))) {
		DIE("[ccack] posix_memalign() failed with %d", ret);
		return -1;
	}
	memset(res, 0, aligned_length(1, MOEPGF_MAX_ALIGNMENT)); // required!

	uint8_t *elem1;
	if ((ret = posix_memalign((void *)&elem1, MOEPGF_MAX_ALIGNMENT, aligned_length(1, MOEPGF_MAX_ALIGNMENT)))) {
		DIE("[ccack] posix_memalign() failed with %d", ret);
		return -1;
	}
	memset(elem1, 0, aligned_length(1, MOEPGF_MAX_ALIGNMENT)); // not necessary, will be overwritten

	// it begins from the right, where the most elements are 0 (in best case we begin just with the pivot element which
	// is 1).
	for (int i = params_s->gensize - 1; i >= 0; --i) {
		if (0 < rlnc_block_get_cv(hashed_cv_rb, i, hcv, cb->cv_len)) {
			/*
			 * We have something like (for an example with gensize = 6 and pivot = 3)
			 *   acv[0] * cv[0] + acv[1] * cv[1] + acv[2] * cv[2] + acv[3] * cv[3] + acv[4] * cv[4] + acv[5] * cv[5] = 0
			 *   <=> (hashed cv is in row echelon form and has 1 at pivot position)
			 *   acv[0] * 0     + acv[1] * 0     + acv[2] * 0     + acv[3] * 1     + acv[4] * cv[4] + acv[5] * cv[5] = 0
			 *   <=>
			 *   acv[3] = - (acv[4] * cv[4] + acv[5] * cv[5])
			 *   <=> (since binary extension field the additive inverse is the same)
			 *   acv[3] = acv[4] * cv[4] + acv[5] * cv[5]
			 */

			memset(res, 0, aligned_length(1, MOEPGF_MAX_ALIGNMENT)); // required!
			memset(elem1, 0, aligned_length(1, MOEPGF_MAX_ALIGNMENT)); // not necessary, will be overwritten

			for (int j = i + 1; j < params_s->gensize; ++j) {
				// elem1 must be set already (but could be set also to 0)
				elem1[0] = get_gf_element(acv, cb->gf.type, j);
				cb->gf.maddrc(res, elem1, get_gf_element(hcv, cb->gf.type, j), 1);
			}

			set_gf_element(acv, params_s->gftype, i, res[0]);
		}
	}

	// The for block is just an assumption check and can be removed.
	/*
	int result = 0;
	for (int i = params_s->gensize - 1; i >= 0; --i) {
		if (0 < rlnc_block_get_cv(hashed_cv_rb, i, hcv, cb->cv_len)) {
			result += dot_prod(hcv, acv, cb->cv_len, &cb->gf);
		}
	} */

	// cleanup
	free(res);
	free(elem1);
	free(hcv);

	LOG(LOG_INFO, "[ccack] ACK coding vector generated: %s", hexdump2(acv, cb->cv_len));

	// Just an assumption check. can be deactivated
	/*
	if (result != 0) {
		DIE("[ccack] assume: ACK coding vector generated wrong");
	} */

	return 0;
}

/**
 * @brief Generates a new Acknowledgement Coding Vector and returns it in \a acv.
 *
 * @ingroup gen_acv
 * @param s Corresponding session struct.
 * @param acv The result as a newly randomly generated Acknowledgement Coding Vector.
 * @return == 0 on success
 * @return != 0 on failure
 * @see generate_acv_solve_eq()
 * @see generate_hashed_cv_rb()
 */
int
generate_acv(const session_t s, u8 *acv)
{
	struct params_session *params_s = session_get_params_session(s);
	int ret = 0;

	// define delta
	rlnc_block_t hashed_cv_rb = rlnc_block_init(params_s->gensize, 0, MEMORY_ALIGNMENT, params_s->gftype);
	if (hashed_cv_rb == NULL) {
		LOG(LOG_ERR, "[ccack] rlnc_block_init() failed");
		return -1;
	}

	if ((ret = generate_hashed_cv_rb(s, hashed_cv_rb))) {
		LOG(LOG_ERR, "[ccack] generate_hashed_cv_rb() failed");
		return ret;
	}


	if ((ret = generate_acv_solve_eq(s, hashed_cv_rb, acv))) {
		LOG(LOG_ERR, "[ccack] generate_acv_solve_eq() failed");
		return ret;
	}

	rlnc_block_free(hashed_cv_rb);

	return 0;
}

/**
 * @brief Prepares the struct ::ncm_hdr_ccack for sending.
 *
 * @ingroup gen_acv
 * @post \a hdr is ready for sending and should not be modified any more.
 * @param s Corresponding session struct.
 * @param sid The session identifier to which \a hdr gets added.
 * @param hdr An empty but allocated header.
 * @return == 0 on success
 * @return != 0 on failure
 * @see generate_acv()
 */
int
ccack_init_ncm_hdr(const session_t s, const u8 sid[2 * IEEE80211_ALEN], struct ncm_hdr_ccack *hdr)
{
	struct ccack_block *cb = session_get_ccack_block(s);
	int ret = 0;

	memcpy(hdr->sid, sid, sizeof(hdr->sid));

	hdr->ctot_db = session_get_ccack_ctot_db();

	u8 *acv = malloc(cb->cv_len);
	memset(acv, 0, cb->cv_len); // not necessary, will be overwritten later

	if ((ret = generate_acv(s, acv))) {
		LOG(LOG_ERR, "[ccack] generate_acv() failed");
		return ret;
	}

	memcpy(hdr->acv, acv, cb->cv_len);

	free(acv);

	return 0;
}

/**
 * @brief Insert a coding vector to a list of cv_buffer_entry elements.
 *
 * @ingroup buffers
 * @param head Head of list to which this entry is to be added
 * @param cv_len Size in bytes of a compressed coding vector
 * @param payload Coding vector to be inserted
 * @return 0 if successful
 * @return -1 otherwise
 */
static int
buffer_add(struct list_head *head, int cv_len, const u8 *payload)
{
	struct cv_buffer_entry *entry;

	if (NULL == (entry = malloc(sizeof(struct cv_buffer_entry) + cv_len))) {
		LOG(LOG_ERR, "malloc() failed");
		return -1;
	}

	entry->counter = 0;
	entry->flags = 0;
	memcpy(entry->cv, payload, cv_len);

	list_add(&entry->list, head);

	return 0;
}

/**
 * @brief Add coding vector to ccack_block::buffer_tx
 *
 * @ingroup buffers
 * @param cb Pointer to a ccack_block containing a ccack_block::buffer_tx
 * @param payload Coding vector to be inserted
 * @return  0 if sucessful
 * @return -1 otherwise
 */
int
ccack_buffer_tx_add(struct ccack_block *cb, const u8 *payload)
{
	if (0 != buffer_add(&cb->buffer_tx.b, cb->cv_len, payload)) {
		return -1;
	}
	cb->buffer_tx.count++;
	LOG(LOG_DEBUG, "[ccack] buffer sent: count: %d, cv: %s, cv_len: %d", cb->buffer_tx.count,
	    hexdump2(payload, cb->cv_len), (int)cb->cv_len);

	return 0;
}

/**
 * @brief Add coding vector to ccack_block::buffer_rx
 *
 * @ingroup buffers
 * @param cb Pointer to a ccack_block containing a ccack_block::buffer_rx
 * @param payload Coding vector to be inserted
 * @return  0 if sucessful
 * @return -1 otherwise
 */
int
ccack_buffer_rx_add(struct ccack_block *cb, const u8 *payload)
{
	if (0 != buffer_add(&cb->buffer_rx.b, cb->cv_len, payload)) {
		return -1;
	}
	cb->buffer_rx.count++;
	LOG(LOG_DEBUG, "[ccack] buffer neighbour: count: %d, cv: %s, cv_len: %d", cb->buffer_rx.count,
	    hexdump2(payload, cb->cv_len), (int)cb->cv_len);

	return 0;
}

/**
 * @brief Reset all values inside a ccack_block
 *
 * This deletes all entries in the coding vector buffers ccack_block::buffer_rx and ccack_block::buffer_tx.
 * Resets the ccack_block::rb_master and ccack_block::rb_slave rlnc_block elements.
 *
 * @pre must be called by ccack_block_cleanup()
 * @pre must be called on generation reset by generation_reset()
 * @param cb Pointer to a ccack_block
 * @return 0 if successful
 */
int
ccack_block_reset(struct ccack_block *cb)
{
	struct cv_buffer_entry *cur, *tmp;

	// free sent buffer
	list_for_each_entry_safe(cur, tmp, &cb->buffer_tx.b, list) {
		list_del(&cur->list);
		free(cur);
	}
	cb->buffer_tx.count = 0;

	// free nb buffer
	list_for_each_entry_safe(cur, tmp, &cb->buffer_rx.b, list) {
		list_del(&cur->list);
		free(cur);
	}
	cb->buffer_rx.count = 0;

	// reset rlnc_blocks
	rlnc_block_reset(cb->rb_master);
	rlnc_block_reset(cb->rb_slave);

	LOG(LOG_DEBUG, "[ccack] block reset");

	return 0;
}

/**
 * @brief Process an acknowledgment coding vector
 *
 * The central idea around which CCACK is built is to use acknowledgment vectors to share information about the node's
 * current rank. Thus, nodes around it can compute the what part of the coding sub-space that it possesses. This is done
 * via what is referred to as 'H_tests' in the paper. When a node overhears an acknowledgment vector (\f$z\f$) from
 * a neighbouring node it checks all the vectors that it has in its transmitted and received buffer to determine whether
 * they have been 'heard' or not. The transmitted buffer is denoted as \f$B_w\f$ in the paper and the received buffer
 * is denoted as \f$B_u\f$. A single 'H_test' is successful when the dot product of a hashed coding vector and the
 * acknowledgment vector is 0. This 'H_test' is performed using the <B>hash matrices of the remote node</B>. Thus, for
 * all hash matrices the 'H_tests' can be succinctly presented as: \f$\forall j = 1, \cdots, M; uH_jz^T = 0\f$.
 *
 * Implementation
 * --------------
 * This function is called from the main ncm module. When a frame with CCACK header is received on the radio interface
 * and it contains an acknoledgment vector (ACV), everything is passed to this function for further processing. This
 * iterates through all the buffer elements in ccack_block::buffer_rx (\f$B_u\f$) and ccack_block:buffer_tx (\f$B_w\f$).
 * Hashes each of them using the remote node's hash matrices and then calculates the dot product using dot_product().
 * Each buffer is checked in its own loop which in turn contains another loop which iterates through the hash matrices
 * of the remote node. The node which passes the 'H_test' is added to the rlnc_block of the respective direction (ccack_block::rb_master
 * and ccack_block::rb_slave) based on the information provided by get_direction().
 *
 * This function is also responsible for updating the nodes total differential backlog which is part of the CCACK header.
 *
 * @param s Pointer to a session
 * @param local_addr Hardware address of this node (local)
 * @param remote_addr Hardware address of the remote node
 * @param ctot_db Current total differential backlog
 * @param acv Acknowledgment coding vector
 */
void
ccack_process_acv(session_t s, const u8 *local_addr, const u8 *remote_addr, u32 ctot_db, const u8 *acv)
{
	struct params_session *params_s = session_get_params_session(s);
	struct ccack_block *cb = session_get_ccack_block(s);
	/*print_list(params_s->ccack.neighbours);*/
	LOG(LOG_DEBUG, "[ccack] received acv from %s, ctot_db: %d", hexdump2(remote_addr, IEEE80211_ALEN), ctot_db);
	hexdump(acv, cb->cv_len);

	struct ccack_hash_matrices *hm = get_neighbour_hash_matrices(params_s, params_s->ccack.neighbours, remote_addr);

	struct cv_buffer_entry *coding_vector;
	// H_tests for nb buffer
	LOG(LOG_DEBUG, "[ccack] starting H_tests for cv in neighbours buffer");
	list_for_each_entry(coding_vector, &cb->buffer_rx.b, list) {
		u8 all_res = 0;
		for (int i = 0; i < params_s->ccack.hash_matrices_num; ++i) {
			u8 hash[cb->cv_len];
			hadamard_vec_vec_prod(&hash[0], &coding_vector->cv[0], hm->array[i], params_s->gensize, &cb->gf);
			all_res += dot_prod(&hash[0], acv, params_s->gensize, &cb->gf);
		}

		// if vector passes H_tests then update
		if (!all_res) {
			LOG(LOG_DEBUG, "[ccack] cv in neighbour buffer passed H_tests");
			hexdump(&coding_vector->cv[0], cb->cv_len);
			u8 sid[2 * IEEE80211_ALEN];
			session_get_sid(&sid[0], s);

			uint8_t dir = get_direction(local_addr, remote_addr, &sid[0]);
			coding_vector->flags |= dir;
			// add to the rlnc_block in the correct direction
			if (dir & DIR_TOWARDS_SLAVE)
				rlnc_block_decode(cb->rb_slave, &coding_vector->cv[0], cb->cv_len);
			else if (dir & DIR_TOWARDS_MASTER)
				rlnc_block_decode(cb->rb_master, &coding_vector->cv[0], cb->cv_len);
			else
				DIE("[ccack] unknown direction");
		}
	}

	// H_tests for sent buffer
	LOG(LOG_DEBUG, "[ccack] starting H_tests for cv in sent buffer");
	list_for_each_entry(coding_vector, &cb->buffer_tx.b, list) {
		u8 all_res = 0;
		for (int i = 0; i < params_s->ccack.hash_matrices_num; ++i) {
			u8 hash[cb->cv_len];
			hadamard_vec_vec_prod(&hash[0], &coding_vector->cv[0], hm->array[i], params_s->gensize, &cb->gf);
			all_res += dot_prod(&hash[0], acv, params_s->gensize, &cb->gf);
		}

		// if vector passes H_tests then update
		if (!all_res) {
			LOG(LOG_DEBUG, "[ccack] cv in sent buffer passed H_tests");
			hexdump(&coding_vector->cv[0], cb->cv_len);
			u8 sid[2 * IEEE80211_ALEN];
			session_get_sid(&sid[0], s);

			uint8_t dir = get_direction(local_addr, remote_addr, &sid[0]);
			coding_vector->flags |= dir;
			// add to the rlnc_block in the correct direction
			if (dir & DIR_TOWARDS_SLAVE)
				rlnc_block_decode(cb->rb_slave, &coding_vector->cv[0], cb->cv_len);
			else if (dir & DIR_TOWARDS_MASTER)
				rlnc_block_decode(cb->rb_master, &coding_vector->cv[0], cb->cv_len);
			else
				DIE("[ccack] unknown direction");
		}
	}

	update_tot_db(params_s, ctot_db);
}

/**
 * @brief Destructor for ccack_block
 *
 * @param cb Pointer to a ccack_block
 */
void
ccack_block_cleanup(struct ccack_block *cb)
{
	ccack_block_reset(cb);
	rlnc_block_free(cb->rb_master);
	rlnc_block_free(cb->rb_slave);
	free(cb);

	LOG(LOG_DEBUG, "[ccack] ccack_block cleanup complete");
}

/**
 * @brief Perform global ccack_cleanup
 *
 * This deletes the hash matrices that are stored globally in the session parameters along with all the neighbour
 * hash matrices that are generated during the operation.
 *
 * @param cc Pointer to the params_session::ccack in session parameters struct
 */
void
ccack_cleanup(struct ccack *cc)
{
	// free hash_matrices_own
	free(cc->hash_matrices_own->array);
	free(cc->hash_matrices_own);

	// free neighbours list
	struct ccack_neighbour *cur, *tmp;
	list_for_each_entry_safe(cur, tmp, cc->neighbours, list) {
		list_del(&cur->list);
		free(cur->hm->array);
		free(cur->hm);
		free(cur);
	}
}

/**
 * @brief Returns the rank (no. of linearly independent coding vectors that were acknowledged in the direction towards
 * master.
 *
 * @ingroup buffers
 * @param cb Pointer to a ccack_block
 * @return Rank of the ccack_block::rb_master rlnc_block
 */
int
ccack_block_master_rlnc_rank(struct ccack_block *cb)
{
	return rlnc_block_rank_decode(cb->rb_master);
}

/**
 * @brief Returns the rank (no. of linearly independent coding vectors that were acknowledged in the direction towards
 * slave.
 *
 * @param cb Pointer to a ccack_block
 * @return Rank of the ccack_block::rb_slave rlnc_block
 */
int
ccack_block_slave_rlnc_rank(struct ccack_block *cb)
{
	return rlnc_block_rank_decode(cb->rb_slave);
}

/**
 * @brief Calculate timeout for cb_rtx()
 *
 * This function repurposes the rate control algorithm proposed in the paper to generate a timeout value for
 * cb_rtx(). Before we go into the details here is list of important terms:
 *
 * - Differential backlog:
 * 		A node's differential backlog (\f$\Delta Q^f\f$) is the difference between the number of innovative
 * 		packets that it has received and the number of innovative packets that neighbours have acknowledged. ccack_get_db()
 * 		calculates this value for a generation.
 * - Total differential backlog:
 * 		This value signifies the total differential backlog of all the neighbour nodes for all flows. Each time
 * 		a node \f$n_j\f$ transmits it also includes in the header, its current total differential backlog of all
 * 		flows crossing that node, \f$\Delta Q^{tot}_{n_j}\f$. This value is made available through the ncm_hdr_ccack::ctot_db in
 * 		the CCACK header. update_tot_db() is used to maintain a local total differential backlog as an exponential moving average.
 * 		It is denoted as \f$\Delta Q_N\f$ symbollically.
 * - Relative differential backlog:
 * 		This is calculated as \f$\Delta Q^f_{rel} = \frac{\Delta Q^f}{\Delta Q^f + \Delta Q_N}\f$. ccack_get_rel_db()
 * 		calculates this value for a generation.
 *
 * 	Implementation
 * 	--------------
 *
 * 	\f$\Delta Q^f\f$ is used as a stopping condition for the cb_rtx() callback, i.e. the timeout is cancelled if the node has
 * 	no new information to share. This function uses \f$\Delta Q^f_{rel}\f$ to calculate a timeout value. The relative differential
 * 	backlog is always in \f$[0, 1]\f$. The idea is that higher the value, the more data a node has to send. Thus, the timeout is
 * 	calculated from 1 - \f$\Delta Q^f\f$ and scaling it to values in [#GENERATION_RTX_MAX_TIMEOUT, #GENERATION_RTX_MIN_TIMEOUT	].
 *
 * @ingroup db
 * @param g pointer to a generation
 * @return pointer to an itimerspec
 */
struct itimerspec *
ccack_rtx_timeout(const generation_t g)
{
	float t;
	t = 1 - ccack_get_rel_db(g);
	/*t = 1 - (0.5 * (ccack_get_rel_db(g) + ((rand() & 0xff)  / 0xff)));*/
	t = (GENERATION_RTX_MAX_TIMEOUT - GENERATION_RTX_MIN_TIMEOUT) * t + GENERATION_RTX_MIN_TIMEOUT;
	LOG(LOG_ERR, "[ccack] timeout value: %d", (int)t);
	return timeout_msec((int)t, 0);
}
