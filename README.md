Cumulative Coded Acknowledgments (CCACK)
=========================================

Wireless mesh networks can make use of Network Coding (NC) to improve the throughput. This is an [implementation](moep80211ncm/src/ccack.h) based on [“Efficient Network-Coding-Based Opportunistic Routing Through Cumulative Coded Acknowledgments”](https://doi.org/10.1109/TNET.2011.2111382). Forwarders in the mesh network can now acknowledge its received data with single fixed-size coding vector, so that it’s neighbours can stop sending if enough nodes in the direction of the destination has received all data to send. This implementation is improved so that it allows bidirectional communication. For an overview about the algorithm have a look at the [slides](project-slides/slides-nc.pdf). 


## Authors

Fabian Raab <fabian.raab@tum.de>
Jait Dixit <jait.dixit@tum.de>

## Branches

We have two implementations based on how retransmits are handled. 'master' contains the implementation which
uses relative differential backlog to reschedule the retransmits inside a generation. This mechanism is adapted
from the rate control algorithm presented in the paper. 'experimental' uses the ncm module's vanilla retransmit
scheme which uses rate-adaptive link quality estimation (ralqe).

## Documentation

Can be viewed via Doxygen. Requires doxygen latex. Just run

    doxygen ./Doxyfile

Then just open `doxygen/html/index.html` in your browser.
Note that many functions of ccack.c are grouped in the moep80211ncm/Modules section of doxygen.

## Quick start

compile the moep80211ncm module like in its README. CCACK can be activated via the `--ccack` parameter, like:

    ./ncm --ccack wlp1s0 2412M

for more information call `./ncm --help`.

have also a look at `setup_venv.py` in `tools`, which makes the set up of virtual wireless networks easy.

## Contribution rules

The main file is `moep80211ncm/src/ccack.c` which is also a good point to start inspecting.

### Linux Kernel styleguide

Every FILE must pass the command

    astyle --options=astyle.rc FILE


where `astyle.rc` is in the project root.
You can also set the options file the environment variable

    ARTISTIC_STYLE_OPTIONS

