#!/usr/bin/python3

from pylab import plot, show, savefig, xlim, figure, \
                hold, ylim, legend, boxplot, setp, axes
import numpy as np

import matplotlib.patches as mpatches

# Some fake data to plot
A= [[1, 2, 5,],  [7, 2, 8]]
B = [[5, 7, 2, 2, 5], [7, 2, 5]]
C = [[3,2,5,7], [6, 7, 3]]

# print(A.shape)

fig = figure()
ax = axes()
hold(True)

ccack_data = np.loadtxt(
        open("rounds_10_count_300/evaluation_ccack.csv", "rb"),
        delimiter=";", skiprows=1
    )

ralqe_data = np.loadtxt(
        open("rounds_10_count_300/evaluation_ralqe.csv", "rb"),
        delimiter=";", skiprows=1
    )

A = [ccack_data[:,1], ralqe_data[:,1]]
B = [ccack_data[:,2], ralqe_data[:,2]]
C = [ccack_data[:,3], ralqe_data[:,3]]
# print(A.shape)

print(ccack_data)

ccack_color = 'lightgreen'
ralqe_color = 'lightblue'

# first boxplot pair
bp = boxplot(A, positions = [1, 2], widths = 0.6, patch_artist=True)
bp['boxes'][0].set_facecolor(ccack_color)
bp['boxes'][1].set_facecolor(ralqe_color)

# second boxplot pair
bp = boxplot(B, positions = [4, 5], widths = 0.6, patch_artist=True)
bp['boxes'][0].set_facecolor(ccack_color)
bp['boxes'][1].set_facecolor(ralqe_color)

# thrid boxplot pair
bp = boxplot(C, positions = [7, 8], widths = 0.6, patch_artist=True)
bp['boxes'][0].set_facecolor(ccack_color)
bp['boxes'][1].set_facecolor(ralqe_color)

# set axes limits and labels
xlim(0,9)
# ylim(0,9)
ax.set_xticklabels(['master', 'forwarder', 'slave'])
ax.set_xticks([1.5, 4.5, 7.5])

ax.set_ylabel('amount of data packets sent')

ccack_patch = mpatches.Patch(color=ccack_color, label='CCACK')
ralqe_patch = mpatches.Patch(color=ralqe_color, label='RALQE')
legend(handles=[ccack_patch, ralqe_patch], loc=2)

savefig('boxcompare.png')
show()
